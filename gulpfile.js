//var param      = require('gulp-param');
var gulp       = require('gulp');
var useref     = require('gulp-useref')
var gulpif     = require('gulp-if')
var uglify     = require('gulp-uglify')
var clean      = require('gulp-clean');
var ftp        = require('gulp-ftp');
var imagemin   = require('gulp-imagemin');
var pngquant   = require('imagemin-pngquant');
var changed    = require('gulp-changed');
var less       = require('gulp-less');
var path       = require('path');
var handlebars = require('gulp-compile-handlebars');
var rename     = require('gulp-rename');
var data       = require('gulp-data');
var pages      = require('./src/data.json');

// Для тестов
//var file       = require('gulp-file')
//var gap        = require('gulp-append-prepend');
//var prompt     = require('gulp-prompt');
//var newBem     = require('gulp-new-bem');

gulp.task('test-file', function() {
  var str = "";
  
  return file('test.less', str, { src: true })
    .pipe(gulp.dest('src/pre-build/less/'));
});

gulp.task('test-bem', function() {
    var str = "";
  return file('test.less', str)
    .pipe(gulp.dest('src/pre-build/less/components/'+ str));
});

/*
gulp.task('promt', function() {
  gulp.src('createBlock.js')
    .pipe(prompt.prompt({
        type: 'input',
        name: 'task',
        message: 'Which task would you like to run?'
    }, function(res){
        console.log(res);
        console.log(res.task);
    }));
});
*/
gulp.task('handlebars', function() { 
    var options = {
            batch : ['./src/partials']
        }
    for(var i=0; i<pages.length; i++) {
        var page = pages[i],
            fileName = page.name;

        gulp.src('src/templates/_' + fileName + '.html')
            .pipe(handlebars(page, options))
            .pipe(rename(fileName + ".html"))
            .pipe(gulp.dest('src/pre-build/'));
    }
    
});

gulp.task('push', ['build'], function (dest) {
    return gulp.src('dist/**')
        .pipe( ftp({
            host: 'boltian.ftp.ukraine.com.ua',
            user: 'boltian_admin',
            pass: 'uI98u9BkG9',
            remotePath : '/boltian.name/movie/'
        })
        )
});

gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

gulp.task('image', function () {
    return gulp.src('src/pre-build/img/*')
        .pipe(changed('dist/img'))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('fonts', function() {
    gulp.src('src/pre-build/fonts/*')
        .pipe(changed('dist/fonts'))
        .pipe(gulp.dest('dist/fonts'))
});
gulp.task('js', function() {
    gulp.src('src/pre-build/js/*')
        .pipe(changed('dist/js'))
        .pipe(gulp.dest('dist/js'))
});
gulp.task('php', function() {
    gulp.src('src/pre-build/*.php')
        .pipe(changed('dist'))
        .pipe(gulp.dest('dist'))
});

gulp.task('copy-css', function() {
    gulp.src('src/pre-build/css/*.css')
        .pipe(changed('dist/css'))
        .pipe(gulp.dest('dist/css'))
});

gulp.task('css', function () {
  gulp.src('src/css/*.css')
});


 
gulp.task('less', function () {
  return gulp.src('./src/pre-build/less/style.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./src/pre-build/css'));
});

gulp.task('less-bootstrap', function () {
  return gulp.src('./src/pre-build/less/bootstrap/bootstrap.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./src/pre-build/css'));
});
 
gulp.task('html', function () {
  gulp.src('src/*.html')
});

gulp.task('build', ['image', 'fonts', 'php', 'js', 'copy-css'], function () {
    var assets = useref.assets();
    
    return gulp.src('src/pre-build/**/*.html')
        .pipe(changed('dist'))
        .pipe(assets)
        .pipe(gulpif('*.js', uglify()))
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(gulp.dest('dist'));
});

 
gulp.task('watch', function () {
  //gulp.watch(['./src/**/*.html'], ['html'])
  gulp.watch(['./src/pre-build/css/*.css'], ['css'])
  gulp.watch(['./src/pre-build/less/**/*.less'], ['less'])
  gulp.watch(['./src/pre-build/less/bootstrap/*.less'], ['less-bootstrap'])
  gulp.watch(['./src/partials/**/*.html'], ['handlebars'])
  gulp.watch(['./src/templates/**/*.html'], ['handlebars'])
});
 
gulp.task('default', ['connect', 'html', 'css', 'watch']);